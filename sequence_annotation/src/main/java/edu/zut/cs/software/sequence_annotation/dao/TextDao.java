package edu.zut.cs.software.sequence_annotation.dao;

import edu.zut.cs.software.sequence_annotation.domain.Text;
import edu.zut.cs.software.sequence_annotation.domain.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TextDao extends JpaRepository<Text, Long> {

}
