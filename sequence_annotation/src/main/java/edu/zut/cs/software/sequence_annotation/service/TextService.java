package edu.zut.cs.software.sequence_annotation.service;

import edu.zut.cs.software.sequence_annotation.domain.Text;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public interface TextService {

    @Transactional
    void saveAll(List<Text> texts);

    List<Text> findAll();

    Optional<Text> findById(long id);

}
