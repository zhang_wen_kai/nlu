package edu.zut.cs.software.sequence_annotation.domain;

public class Auditor {
    private String taskNumber;
    private String corpus;
    private String phases;
    private String annotateTaskType;
    private String state;
    private String verifier;
    private String markPart;
    private String submissionTime;

    public String getTaskNumber() {
        return taskNumber;
    }

    public void setTaskNumber(String taskNumber) {
        this.taskNumber = taskNumber;
    }

    public String getCorpus() {
        return corpus;
    }

    public void setCorpus(String corpus) {
        this.corpus = corpus;
    }

    public String getPhases() {
        return phases;
    }

    public void setPhases(String phases) {
        this.phases = phases;
    }

    public String getAnnotateTaskType() {
        return annotateTaskType;
    }

    public void setAnnotateTaskType(String annotateTaskType) {
        this.annotateTaskType = annotateTaskType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVerifier() {
        return verifier;
    }

    public void setVerifier(String verifier) {
        this.verifier = verifier;
    }

    public String getMarkPart() {
        return markPart;
    }

    public void setMarkPart(String markPart) {
        this.markPart = markPart;
    }

    public String getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(String submissionTime) {
        this.submissionTime = submissionTime;
    }
}