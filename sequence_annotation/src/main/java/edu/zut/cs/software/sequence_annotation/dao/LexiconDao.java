package edu.zut.cs.software.sequence_annotation.dao;

import edu.zut.cs.software.sequence_annotation.domain.Lexicon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LexiconDao extends JpaRepository<Lexicon,Long> {

}
