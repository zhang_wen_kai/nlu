package edu.zut.cs.software.sequence_annotation.service.impl;

import edu.zut.cs.software.sequence_annotation.dao.TextDao;
import edu.zut.cs.software.sequence_annotation.domain.Text;
import edu.zut.cs.software.sequence_annotation.service.TextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TextServiceImpl implements TextService {
    @Autowired
    public TextDao textDao;

    @Override
    public void saveAll(List<Text> texts) {
        textDao.saveAll(texts);
    }

    @Override
    public List<Text> findAll() {
        return textDao.findAll();
    }

    @Override
    public Optional<Text> findById(long id) {
        return textDao.findById(id);
    }
}
