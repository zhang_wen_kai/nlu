package edu.zut.cs.software.sequence_annotation.service.impl;

import edu.zut.cs.software.sequence_annotation.dao.CorpusDao;
import edu.zut.cs.software.sequence_annotation.domain.Corpus;
import edu.zut.cs.software.sequence_annotation.service.CorpusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CorpusServiceImpl implements CorpusService {

    @Autowired
    CorpusDao corpusDao;

    public void saveAll(List<Corpus> corpuses){
        corpusDao.saveAll(corpuses);
    }

    public List<Corpus> findAll()
    {
        return corpusDao.findAll();
    }

    public Optional<Corpus> findById(Long id){
        return corpusDao.findById(id);
    }
}
