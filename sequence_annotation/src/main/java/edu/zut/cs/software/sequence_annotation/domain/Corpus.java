package edu.zut.cs.software.sequence_annotation.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * 语料库实体
 */
@Entity(name = "T_CORPUS")
public class Corpus {
    @Id
    private Long id;

    //语料类型（中文分词、词性标注、命名实体、专业术语、依存关系、语义角色......）
    @Column(length = 20)
    private String corporaTypes;

    //语料数据类型（妇产科、消化科、儿科......）
    @Column(length = 20)
    private String corpusDataTypes;

    //保存数据的文本或者？？？？,(存在疑问)
    @Column(length = 20)
    private String corpusData;

    //创建人
    @Column(length = 20)
    private String creator;

    //创建时间
    @Column(length = 20)
    private Date creatTime;


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public String getCorporaTypes() {
        return corporaTypes;
    }

    public void setCorporaTypes(String corporaTypes) {
        this.corporaTypes = corporaTypes;
    }

    public String getCorpusDataTypes() {
        return corpusDataTypes;
    }

    public void setCorpusDataTypes(String corpusDataTypes) {
        this.corpusDataTypes = corpusDataTypes;
    }

    public String getCorpusData() {
        return corpusData;
    }

    public void setCorpusData(String corpusData) {
        this.corpusData = corpusData;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }
}
