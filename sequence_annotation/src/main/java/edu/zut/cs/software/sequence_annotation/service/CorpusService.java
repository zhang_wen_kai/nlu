package edu.zut.cs.software.sequence_annotation.service;

import edu.zut.cs.software.sequence_annotation.domain.Corpus;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * 语料库的service类
 */
@Service
public interface CorpusService {

    @Transactional
    public void saveAll(List<Corpus> corpuses);

    public List<Corpus> findAll();

    public Optional<Corpus> findById(Long id);

}
