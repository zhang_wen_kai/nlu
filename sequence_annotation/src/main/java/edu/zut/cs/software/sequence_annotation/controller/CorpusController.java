package edu.zut.cs.software.sequence_annotation.controller;


import edu.zut.cs.software.sequence_annotation.domain.Corpus;
import edu.zut.cs.software.sequence_annotation.domain.Word;
import edu.zut.cs.software.sequence_annotation.service.CorpusService;
import edu.zut.cs.software.sequence_annotation.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/CorpusController")
public class CorpusController {

    @Autowired
    private CorpusService corpusService;

    @GetMapping("/all")
    public List<Corpus> mad(){
        return corpusService.findAll();
    }

    @RequestMapping("/id")
    public Optional<Corpus> findById(Long id){
        return corpusService.findById(id);
    }

}
