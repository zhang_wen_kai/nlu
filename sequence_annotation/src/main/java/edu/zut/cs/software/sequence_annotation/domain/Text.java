package edu.zut.cs.software.sequence_annotation.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "T_TEXT")
public class Text {
    @Id
    private int textNumber;
    @Column
    private String textName;
    @Column
    private Long textLength;

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public void setTextLength(Long textLength) {
        this.textLength = textLength;
    }

    public void setTextNumber(int textNumber) {
        this.textNumber = textNumber;
    }

    public String getTextName() {
        return textName;
    }

    public Long getTextLength() {
        return textLength;
    }

    public int getTextNumber() {
        return textNumber;
    }
}
