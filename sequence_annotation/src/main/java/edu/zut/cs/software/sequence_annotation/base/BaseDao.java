package edu.zut.cs.software.sequence_annotation.base;

import edu.zut.cs.software.sequence_annotation.base.util.Page;

import edu.zut.cs.software.sequence_annotation.base.util.PageList;
import edu.zut.cs.software.sequence_annotation.base.util.PageUtil;
import org.hibernate.*;
import org.hibernate.transform.Transformers;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BaseDao<T, PK extends Serializable> {

    // 泛型反射类
    private Class<T> entityClass;

    // 通过反射获取子类确定的泛型类
    @SuppressWarnings({"rawtypes", "unchecked"})
    public BaseDao() {
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        entityClass = (Class) params[0];
    }

    @PersistenceContext
    private EntityManager entityManager;

    private SessionFactory sessionFactory;

    public void setSessionFactory() {
        this.sessionFactory = entityManager.unwrap(Session.class).getSessionFactory();
    }

    public SessionFactory getSessionFactory() {
        return entityManager.unwrap(Session.class).getSessionFactory();
        //return entityManager.unwrap(Session.class).getSessionFactory();
    }

    public Session getSession() {
        return entityManager.unwrap(Session.class);//.getSessionFactory().getCurrentSession();
    }

    /**
     * 方法功能说明：保存PO 创建： 修改内容：
     *
     * @return PK
     * @throws
     * @参数： @param entity
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public PK save(T t) {
        return (PK) getSession().save(t);
    }

    /**
     * 方法功能说明：延迟加载PO 创建  修改内容：
     *
     * @return T
     * @throws
     * @参数： @param id
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public T load(PK id) {
        return (T) getSession().load(entityClass, id);
    }

    /**
     * 方法功能说明：获取PO 创建  修改内容：
     *
     * @return T
     * @throws
     * @参数： @param id
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public T get(PK id) {
        return (T) getSession().get(entityClass, id);
    }

    /**
     * 方法功能说明：修改对象 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param t
     * @参数： @throws Exception
     */
    public void update(T t) {
        getSession().update(t);
    }

    /**
     * 方法功能说明：保存或修改PO 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param t
     * @参数： @throws Exception
     */
    public void merge(T t) throws Exception {
        getSession().merge(t);
    }

    /**
     * 方法功能说明：删除PO 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param t
     * @参数： @throws Exception
     */
    public void delete(T t) {
        getSession().delete(t);
    }

    /**
     * 方法功能说明：主键删除 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param id
     * @参数： @throws Exception
     */
    public void deleteById(PK id) {
        T ret = this.load(id);
        if (ret != null) {
            delete(ret);
        }
    }

    public boolean exists(PK id) {
        return get(id) != null;
    }

    /**
     * 方法功能说明：查询HQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return List<T>
     * @throws
     * @参数： @param hql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List findList(String hql) {
        return getSession().createQuery(hql).list();
    }

    /**
     * 方法功能说明：查询HQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @param hql
     * @param number
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<T> findListByNum(String hql, int number) {
        return getSession().createQuery(hql).setMaxResults(number).list();
    }

    /**
     * 方法功能说明：查询HQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return List<T>
     * @throws
     * @参数： @param hql
     * @参数： @return
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    public List<T> findListLock(String hql, String tableName) {
        Query query = getSession().createQuery(hql);
        query.setLockMode(tableName, LockMode.UPGRADE); // 加锁
        return query.list();// 执行查询，获取数据

    }

    /**
     * 方法功能说明：带参数的查询功能    修改内容：
     *
     * @return List<T>
     * @throws
     * @参数： @param hql
     * @参数： @param params
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List<T> findList(String hql, Object... params) {
        Query query = getSession().createQuery(hql);
        this.setParameter(query, params);
        return query.list();
    }

    /**
     * 方法功能说明：带参数的查询功能  修 修改内容：
     *
     * @return List<T>
     * @throws
     * @参数： @param hql
     * @参数： @param params
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List<T> findList(String hql, String[] params, Object[] values) {
        Query query = getSession().createQuery(hql);
        this.setParameter(query, params, values);
        return query.list();
    }

    /**
     * 方法功能说明：执行SQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return int
     * @throws
     * @参数： @param sql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List<T> findEntityListBySql(String sql, final Class<T> clzz) {
        return getSession().createSQLQuery(sql).addEntity(clzz).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findEntityListBySql(String sql, final Class<T> clzz, Object... params) {
        Query query = getSession().createSQLQuery(sql).addEntity(clzz);
        this.setParameter(query, params);
        return query.list();
    }

    /**
     * 方法功能说明：带参数的查询功能
     *
     * @return List<T>
     * @throws
     * @参数： @param hql
     * @参数： @param params
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List<T> findEntityListBySql(String sql, final Class<T> clzz, String[] params, Object[] values) {
        Query query = getSession().createSQLQuery(sql).addEntity(clzz);
        this.setParameter(query, params, values);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> findListBySqlNoEntity(String sql) {
        return getSession().createSQLQuery(sql)
                .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
    }

    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> findListBySqlNoEntity(String sql, Object... params) {
        Query query = getSession().createSQLQuery(sql);
        this.setParameter(query, params);
        return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();

    }

    @SuppressWarnings("unchecked")
    public List<LinkedHashMap<String, Object>> findLinkedHashMapList(String sql, Object... params) {
        Query query = getSession().createSQLQuery(sql);
        this.setParameter(query, params);
        return query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();

    }

    /**
     * 方法功能说明：查询SQL 返回MAP SQL的字段为KEY 值为VALUE 创建：2020-4-26 by
     * 修改：2020-4-26 by  修改内容：
     *
     * @return List<Map < String, Object>>
     * @throws
     * @参数： @param sql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> findListBySql(String sql) {
        return getSession().createSQLQuery(sql)
                .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
    }


    @SuppressWarnings({"unchecked"})
    public List<Object> findListBySqlNoMap(String sql) {
        return getSession().createSQLQuery(sql).list();
    }

    /**
     * 方法功能说明：执行HQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return int
     * @throws
     * @参数： @param sql
     * @参数： @return
     */
    public int executeHql(String hql) {
        return getSession().createQuery(hql).executeUpdate();
    }

    /**
     * 方法功能说明：执行SQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return int
     * @throws
     * @参数： @param sql
     * @参数： @return
     */
    public int executeSql(String sql) {
        return getSession().createSQLQuery(sql).executeUpdate();
    }

    /**
     * 方法功能说明：执行SQL 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return int
     * @throws
     * @参数： @param sql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public int executeSql(String sql, String[] params, Object[] values) {
        Query query = getSession().createSQLQuery(sql);
        this.setParameter(query, params, values);
        return query.executeUpdate();
    }

    /**
     * 方法功能说明：HQL查询LIST数量 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return int
     * @throws
     * @参数： @param hql
     * @参数： @param objects
     * @参数： @return
     */
    public int count(String hql) {
        Query query = getSession().createQuery(hql);
        ScrollableResults sr = query.scroll();
        sr.last();
        return sr.getRowNumber() == -1 ? 0 : sr.getRowNumber() + 1;
    }

    /**
     * 方法功能说明：HQL查询LIST数量(带参数) 创建：2020-4-26 by  修改：2020-4-26 by
     * 修改内容：
     *
     * @return int
     * @throws
     * @参数： @param hql
     * @参数： @param objects
     * @参数： @return
     */
    public int countObjects(String hql, Object... objects) {
        Query query = getSession().createQuery(hql);
        setParameter(query, objects);
        ScrollableResults sr = query.scroll();
        sr.last();
        return sr.getRowNumber() == -1 ? 0 : sr.getRowNumber() + 1;
    }

    /**
     * 方法功能说明：HQL分页查询 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return PageList<T>
     * @throws
     * @参数： @param page
     * @参数： @param hql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public PageList<T> findPageListByHql(Page page, String hql) {
        Query query = getSession().createQuery(hql);
        ScrollableResults sr = query.scroll();
        sr.last();
        int count = sr.getRowNumber() == -1 ? 0 : sr.getRowNumber() + 1;
        query.setFirstResult(PageUtil.getBeginIndex(page.getEveryPage(),
                page.getCurrentPage()));
        query.setMaxResults(PageUtil.getEveryPage(page.getEveryPage()));
        page.setTotalCount(count);
        Page p = PageUtil.createPage(page.getEveryPage(), page.getTotalCount(),
                page.getCurrentPage());
        return new PageList<T>(p, query.list());
    }

    /**
     * 方法功能说明：HQL分页查询 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return PageList<T>
     * @throws
     * @参数： @param page
     * @参数： @param hql
     * @参数： @return
     */
    @SuppressWarnings("unchecked")
    public PageList<T> findPageListByHql(Page page, String hql,
                                         Object... objects) {
        Query query = getSession().createQuery(hql);
        setParameter(query, objects);
        ScrollableResults sr = query.scroll();
        sr.last();
        int count = sr.getRowNumber() == -1 ? 0 : sr.getRowNumber() + 1;
        query.setFirstResult(PageUtil.getBeginIndex(page.getEveryPage(),
                page.getCurrentPage()));
        query.setMaxResults(page.getEveryPage());
        page.setTotalCount(count);
        Page p = PageUtil.createPage(page.getEveryPage(), page.getTotalCount(),
                page.getCurrentPage());
        return new PageList<T>(p, query.list());
    }

    /**
     * 方法功能说明：传入参数 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param query
     * @参数： @param objects
     */
    void setParameter(Query query, Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            query.setParameter(i, objects[i]);
        }
    }

    /**
     * 方法功能说明：传入参数 创建：2020-4-26 by  修改：2020-4-26 by  修改内容：
     *
     * @return void
     * @throws
     * @参数： @param query
     * @参数： @param objects
     */
    @SuppressWarnings("rawtypes")
    void setParameter(Query query, String[] params, Object[] values) {
        for (int i = 0; i < params.length; i++) {
            if (values[i].getClass().equals(String[].class)) {
                query.setParameterList(params[i], (String[]) values[i]);
            } else {
                query.setParameter(params[i], values[i]);
            }
        }
    }

}