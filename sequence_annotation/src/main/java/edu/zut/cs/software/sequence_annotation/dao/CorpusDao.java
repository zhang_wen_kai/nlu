package edu.zut.cs.software.sequence_annotation.dao;

import edu.zut.cs.software.sequence_annotation.domain.Corpus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 语料库的数据接口dao层
 */
@Repository
public interface CorpusDao extends JpaRepository<Corpus,Long> {
}
