package edu.zut.cs.software.sequence_annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude = MongoAutoConfiguration.class)
public class SequenceAnnotationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SequenceAnnotationApplication.class, args);
    }

}
