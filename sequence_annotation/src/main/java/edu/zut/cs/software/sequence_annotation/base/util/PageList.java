package edu.zut.cs.software.sequence_annotation.base.util;

import java.util.List;

public class PageList<T> {
	private Page page;  
	  
    private List<T> list;  
  
    /** 
     * The default constructor 
     */  
    public PageList(){  
        super();
    }  
  
    /** 
     * The constructor using fields 
     *  
     * @param page
     */  
    public PageList(Page page, List<T> list) {  
        this.page = page;  
        this.list = list;  
    }

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}  

}
