package edu.zut.cs.software.sequence_annotation.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.Date;

/**
 * 全局异常处理器
 */
@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<Response> handlerException(Exception e){
        String msg = "抱歉,服务器繁忙,请稍后重试";
        int status = HttpStatus.BAD_REQUEST.value();
        if (e instanceof GlobalException) {
            msg = e.getMessage();
            status = ((GlobalException) e).getStatus();
        }
        e.printStackTrace();
        return ResponseEntity.status(status).body(new Response(msg,status));
    }

    class Response {
        private String msg;
        private Date time = new Date();
        private Integer status;

        public Response(){}

        public Response(String msg,Integer status){
            this.msg = msg;
            this.status = status;
        }
        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public Date getTime() {
            return time;
        }

        public void setTime(Date time) {
            this.time = time;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }


}
